package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        //Makes map to store numbers in list and keeps count of them
        Map<T, Integer> map = new HashMap<T, Integer>();

        //Goes through every number in list and either adds it to map or adds to existing count
        for (T number : list){
            if (!map.containsKey(number)) map.put(number, 1);
            else if (map.containsKey(number)){
                int newCount = map.get(number) + 1;

                    //Returns if the count has reached 3
                    if (newCount == 3) return number;
                map.put(number, map.get(number) + 1);
            }
        }

        //Returns null if there is not a triplicate
        return null;

    }
    
}
